# GraphSynth

Generate random multimodal knowledge graph for machine learning benchmark purposes. Note that the structure is left random on purpose to facilitate testing on modalities.


```
usage: generate.py [-h] -n NUM_NODES -r NUM_RELATIONS -t NUM_TARGETS [-c NUM_CLASSES] [-p CLASS_DISTRIBUTION [CLASS_DISTRIBUTION ...]]

optional arguments:
  -h, --help            show this help message and exit
  -n NUM_NODES, --num_nodes NUM_NODES
                        Number of nodes
  -r NUM_RELATIONS, --num_relations NUM_RELATIONS
                        Number of object type properties
  -t NUM_TARGETS, --num_targets NUM_TARGETS
                        Number of target nodes
  -c NUM_CLASSES, --num_classes NUM_CLASSES
                        Number of target classes
  -p CLASS_DISTRIBUTION [CLASS_DISTRIBUTION ...], --class_distribution CLASS_DISTRIBUTION [CLASS_DISTRIBUTION ...]
                        Target class distribution
```
