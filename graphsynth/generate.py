#!/usr/bin/env python

import argparse
from base64 import urlsafe_b64encode
import gzip
from io import BytesIO

import numpy as np

from graphsynth import noise, target
from graphsynth.rdf import Triple, URIRef, Literal


# vocabulary retrieved from http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain
_STRING_VOCAB = "./vocab.txt.gz"
_ATTR_AVAILABLE = ["xsd.string", "xsd.gYear", "xsd.integer", "xsd.float",
                   "xsd.boolean", "ogc.wktLiteral", "blob.image", "xsd.date",
                   "xsd.dateTime", "xsd.anyURI"]
_ATTR_INCLUDED = [attr for attr in _ATTR_AVAILABLE]
_NAMESPACE = "https://example.org/"
_NAMESPACE_XSD = "http://www.w3.org/2001/XMLSchema#"
_NAMESPACE_OGC = "http://www.opengis.net/ont/geosparql#"
_NAMESPACE_KGB = "http://kgbench.info/dt#"

def im_to_b64(im):
    buff = BytesIO()
    im.save(buff, format="JPEG")
    b64img = urlsafe_b64encode(buff.getvalue())

    return b64img.decode()

def gen_vocab():
    vocab = list()
    with gzip.open(_STRING_VOCAB, 'rt') as f:
        vocab = [word for word in f.read().split('\n') if len(word) >= 5]

    return vocab

def gen_random_attr(attr, vocab):
    if attr == "xsd.string":
        o = Literal(noise.gen_string(vocab), dtype=_NAMESPACE_XSD+"string")
    elif attr == "xsd.anyURI":
        o = Literal(noise.gen_anyURI(vocab), dtype=_NAMESPACE_XSD+"anyURI")
    elif attr == "xsd.gYear":
        o = Literal(str(noise.gen_gYear()), dtype=_NAMESPACE_XSD+"gYear")
    elif attr == "xsd.date":
        o = Literal(str(noise.gen_date()), dtype=_NAMESPACE_XSD+"date")
    elif attr == "xsd.dateTime":
        o = Literal(str(noise.gen_dateTime()), dtype=_NAMESPACE_XSD+"dateTime")
    elif attr == "xsd.integer":
        o = Literal(str(noise.gen_integer()), dtype=_NAMESPACE_XSD+"integer")
    elif attr == "xsd.float":
        o = Literal(str(noise.gen_float()), dtype=_NAMESPACE_XSD+"float")
    elif attr == "xsd.boolean":
        o = Literal(str(noise.gen_boolean()), dtype=_NAMESPACE_XSD+"boolean")
    elif attr == "blob.image":
        o = Literal(im_to_b64(noise.gen_image()), dtype=_NAMESPACE_KGB+"base64Image")
    elif attr == "ogc.wktLiteral":
        o = Literal(noise.gen_wktLiteral(), dtype=_NAMESPACE_OGC+"wktLiteral")
    else:
        o = Literal("")  # empty literal

    return o

def gen_target_attr(attr, vocab, assignment, nclusters):
    if attr == "xsd.string":
        o = Literal(target.gen_string(vocab, assignment, nclusters), dtype=_NAMESPACE_XSD+"string")
    elif attr == "xsd.anyURI":
        o = Literal(target.gen_anyURI(vocab, assignment, nclusters), dtype=_NAMESPACE_XSD+"anyURI")
    elif attr == "xsd.gYear":
        o = Literal(str(target.gen_gYear(assignment, nclusters)), dtype=_NAMESPACE_XSD+"gYear")
    elif attr == "xsd.date":
        o = Literal(str(target.gen_date(assignment, nclusters)), dtype=_NAMESPACE_XSD+"date")
    elif attr == "xsd.dateTime":
        o = Literal(str(target.gen_dateTime(assignment, nclusters)), dtype=_NAMESPACE_XSD+"dateTime")
    elif attr == "xsd.integer":
        o = Literal(str(target.gen_integer(assignment, nclusters)), dtype=_NAMESPACE_XSD+"integer")
    elif attr == "xsd.float":
        o = Literal(str(target.gen_float(assignment, nclusters)), dtype=_NAMESPACE_XSD+"float")
    elif attr == "xsd.boolean":
        o = Literal(str(target.gen_boolean(assignment)), dtype=_NAMESPACE_XSD+"boolean")
    elif attr == "blob.image":
        o = Literal(im_to_b64(target.gen_image(assignment, nclusters)), dtype=_NAMESPACE_KGB+"base64Image")
    elif attr == "ogc.wktLiteral":
        o = Literal(target.gen_wktLiteral(assignment, nclusters), dtype=_NAMESPACE_OGC+"wktLiteral")
    else:
        o = Literal("")  # empty literal


    return o

def generate(num_instances, num_relations, num_targets,
             num_target_clusters,
             target_cluster_distribution,
             p_attr=0.9):

    assert len(target_cluster_distribution) == num_target_clusters

    context = set()
    train = set()
    test = set()
    valid = set()

    # generate edges between entities
    edges = noise.gen_structure(num_instances)
    for s, o in edges:
        p = np.random.choice(range(num_relations))
        t = Triple(URIRef(_NAMESPACE + "NODE_" + str(s)),
                   URIRef(_NAMESPACE + "edge_" + str(p)),
                   URIRef(_NAMESPACE + "NODE_" + str(o)))

        context.add(t.ntriple().encode())

    # separate target entities from non-target entities
    assert num_instances >= num_targets
    instances = np.arange(num_instances, dtype=np.int32)
    np.random.shuffle(instances)

    target_entities_idx = instances[:num_targets]
    non_target_entities_idx = instances[num_targets:]

    # generate vocabulary
    vocab = gen_vocab()

    # number of relations associated with an attribute
    attr_relations = dict()
    for attr in _ATTR_INCLUDED:
        attr_relations[attr] = np.random.choice(range(1,4))

    # add attributes to non-target entities
    for s in non_target_entities_idx:
        for attr in _ATTR_INCLUDED:
            if np.random.rand() > p_attr:
                continue

            attr_rel = np.random.choice(range(1, attr_relations[attr]+1))

            t = Triple(URIRef(_NAMESPACE + "NODE_" + str(s)),
                       URIRef(_NAMESPACE + "edge_" + str(attr) + '_' + str(attr_rel)),
                       gen_random_attr(attr, vocab))

            context.add(t.ntriple().encode())

    # add attributes to target entities
    targets = [list() for _ in range(num_target_clusters)]
    for s in target_entities_idx:
        assignment = np.random.choice(range(num_target_clusters),
                                      p=target_cluster_distribution)
        for attr in _ATTR_INCLUDED:
            if np.random.rand() > p_attr:
                continue

            attr_rel = np.random.choice(range(1, attr_relations[attr]+1))

            t = Triple(URIRef(_NAMESPACE + "NODE_" + str(s)),
                       URIRef(_NAMESPACE + "edge_" + str(attr) + '_' + str(attr_rel)),
                       gen_target_attr(attr, vocab,
                                       assignment, num_target_clusters))

            context.add(t.ntriple().encode())

        # add target signal
        t = Triple(URIRef(_NAMESPACE + "NODE_" + str(s)),
                   URIRef(_NAMESPACE + "hasClass"),
                   URIRef(_NAMESPACE + "NODE_CLASS_" + str(assignment)))

        targets[assignment].append(t.ntriple().encode())

    # split targets in train, test, valid set using 80-20 rule
    for target_cluster in targets:
        train_split, test_split = np.split(target_cluster,
                                           [int(0.8*len(target_cluster))])
        train_split, valid_split = np.split(train_split,
                                            [int(0.8*len(train_split))])

        train |= set(train_split)
        test |= set(test_split)
        valid |= set(valid_split)

    return (context, (train, test, valid))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--num_instances", help="Number of instances",
                        required=True, default=None)
    parser.add_argument("-r", "--num_relations", help="Number of object type properties",
                        required=True, default=None)
    parser.add_argument("-t", "--num_targets", help="Number of target instances",
                        required=False, default=None)
    parser.add_argument("-c", "--num_classes", help="Number of target classes",
                        required=False, default=2)
    parser.add_argument("-p", "--class_distribution", help="Target class distribution",
                        required=False, nargs='+', type=int, default=None)
    args = parser.parse_args()

    if args.num_targets is None:
        # no unlabeled nodes
        args.num_targets = args.num_instances

    if args.class_distribution is None:
        # assume uniform distribution if none specified
        args.class_distribution = [1/int(args.num_classes)
                                   for _ in range(int(args.num_classes))]

    # remove this as it adds information to the graph's structure due to its
    # limited range
    _ATTR_INCLUDED.remove("xsd.boolean")

    context, (train, test, valid) = generate(int(args.num_instances),
                                             int(args.num_relations),
                                             int(args.num_targets),
                                             int(args.num_classes),
                                             tuple(args.class_distribution))

    with gzip.open("./context.nt.gz", "wb") as gf:
        gf.write(b'\n'.join(context) + b'\n')
    with gzip.open("./train.nt.gz", "wb") as gf:
        gf.write(b'\n'.join(train) + b'\n')
    with gzip.open("./test.nt.gz", "wb") as gf:
        gf.write(b'\n'.join(test) + b'\n')
    with gzip.open("./valid.nt.gz", "wb") as gf:
        gf.write(b'\n'.join(valid) + b'\n')
