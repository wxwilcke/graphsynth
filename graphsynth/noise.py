#!/usr/bin/env python

import networkx as nx
import numpy as np
from PIL import Image, ImageDraw


def gen_structure(n):
    g = nx.connected_watts_strogatz_graph(n, k=4, p=0.2)
    return g.edges()

def gen_string(vocab, min_length=5, max_length=30):
    length = np.random.randint(min_length, max_length)
    return ' '.join(np.random.choice(vocab, size=length))

def gen_anyURI(vocab, min_length=2, max_length=8):
    base_length = np.random.randint(min_length, max_length-1)
    path_length = np.random.randint(1, (max_length-base_length)+1)
    prefix = np.random.choice(['http', 'https', 'ftp', 'ssh', 'nfs'], 1)[0]
    return prefix + '://' + '.'.join(np.random.choice(vocab, size=base_length))\
                          + '/'.join(np.random.choice(vocab, size=path_length))

def gen_gYear(min_year=1970, max_year=2100):
    return np.random.randint(min_year, max_year)

def gen_date(min_year=1970, max_year=2100):
    # omit days after the 28th for simplicity
    return '%04d-%02d-%02d' % (gen_gYear(min_year, max_year),
                         np.random.randint(12)+1,
                         np.random.randint(28)+1)

def gen_dateTime(min_time='00:00:00', max_time='23:59:59'):
    min_time = min_time.split(':')
    max_time = max_time.split(':')
    return '%sT%02d:%02d:%02d' % (gen_date(),
                            np.random.randint(int(min_time[0]), int(max_time[0])+1),
                            np.random.randint(int(min_time[1]), int(max_time[1])+1),
                            np.random.randint(int(min_time[2]), int(max_time[2])+1))

def gen_integer(min_value=-9e5, max_value=9e5):
    return np.random.randint(min_value, max_value)

def gen_float(min_value=-9e5, max_value=9e5):
    return np.random.rand() * np.random.randint(min_value, max_value)

def gen_boolean():
    return np.random.rand() > 0.5

def gen_image(size=(200, 200)):
    width = max(np.random.randint(min(size[0], size[1])//2), 1)
    x0 = np.random.randint(0, size[0])
    x1 = np.random.randint(0, size[0])
    y0 = np.random.randint(0, size[1])
    y1 = np.random.randint(0, size[1])
    im = Image.new("RGB", size, "black")
    draw = ImageDraw.Draw(im)
    draw.line([(x0, y0), (x1, y1)], fill="white", width=width)

    return im

def gen_point(min_lat=-90, max_lat=90, min_lon=-180, max_lon=180):
    return (gen_float(min_lon, max_lon), gen_float(min_lat, max_lat))

def gen_wktLiteral(min_length=4, max_length=16):
    num_points = np.random.randint(min_length, max_length)
    points = [gen_point() for _ in range(num_points-1)]
    points_str = ["{} {}".format(lon, lat) for (lon, lat) in points]
    points_str.append(points_str[0])  # close polygon
    return "POLYGON ((" +\
            ", ".join(points_str) +\
            "))"

