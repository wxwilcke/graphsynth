#!/usr/bin/env python

from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()

setup(
    name='graphsynth',
    version='0.1',
    author='Xander Wilcke',
    author_email='w.x.wilcke@vu.nl',
    url='https://gitlab.com/wxwilcke/graphsynth',
    description='Generate heterogeneous knowledge graph for benchmark use',
    license='GLP3',
    include_package_data=True,
    zip_safe=True,
    install_requires=[
        'networkx',
        'numpy',
        'pillow',
    ],
    packages=['graphsynth'],
)
